xmldiff
=======

Developement has moved to https://framagit.org/zoggy/xmldiff .

Diffs on XML trees.

More information at http://zoggy.github.io/xmldiff .
